//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Book = require('../app/models/book');

//Require the dev-dependencies
let chai = require('chai');
let server = require('../server');
let expect = chai.expect;
let request = require('supertest')

//Our parent block
describe('Books', () => {

	beforeEach(async () =>
		// Before each test we empty the database
		await Book.remove({}).exec()
	)

	/*
	 * Test the /GET route
	 */
	describe('/GET book', () => {
		it('it should GET all the books', async () => {

			const res = await request(server).get('/book')

			expect(res.statusCode).to.equal(200)
			expect(res.body).to.be.a('array');
			expect(res.body).to.have.lengthOf(0);

		});
	});
	/*
	 * Test the /POST route
	 */
	describe('/POST book', () => {
		it('it should not POST a book without pages field', async () => {

			let book = {
				title: "The Lord of the Rings",
				author: "J.R.R. Tolkien",
				year: 1954
			}

			const res = await request(server).post('/book').send(book)

			expect(res.statusCode).to.equal(200)
			expect(res.body).to.be.a('object');
			expect(res.body).to.have.property('errors')
			expect(res.body.errors).to.have.property('pages')
			expect(res.body.errors.pages).to.have.property('kind').to.equal('required');

		});
		it('it should POST a book ', async () => {

			let book = {
				title: "The Lord of the Rings",
				author: "J.R.R. Tolkien",
				year: 1954,
				pages: 1170
			}

			const res = await request(server).post('/book').send(book)

			expect(res.statusCode).to.equal(200)
			expect(res.body).to.be.a('object');

			// Message
			expect(res.body).to.have.property('message').to.equal('Book successfully added!')

			// Check book properties
			expect(res.body.book).to.have.property('title')
			expect(res.body.book).to.have.property('author')
			expect(res.body.book).to.have.property('pages')
			expect(res.body.book).to.have.property('year')
		});
	});
	/*
	 * Test the /GET/:id route
	 */
	describe('/GET/:id book', () => {
		it('it should GET a book by the given id', async () => {

			let book = new Book({ title: "The Lord of the Rings", author: "J.R.R. Tolkien", year: 1954, pages: 1170 });

			const item = await book.save()
			const res = await request(server).get('/book/' + item.id).send(book)

			expect(res.statusCode).to.equal(200)
			expect(res.body).to.be.a('object');

			// Check book properties
			expect(res.body).to.have.property('title')
			expect(res.body).to.have.property('author')
			expect(res.body).to.have.property('pages')
			expect(res.body).to.have.property('year')
			expect(res.body).to.have.property('_id').to.equal(book.id)

		});
	});
	/*
	 * Test the /PUT/:id route
	 */
	describe('/PUT/:id book', () => {
		it('it should UPDATE a book given the id', async () => {

			let book = new Book({ title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1948, pages: 778 })

			const item = await book.save(book)
			const res = await request(server).put('/book/' + item.id).send({ title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1950, pages: 778 })

			expect(res.statusCode).to.equal(200)
			expect(res.body).to.be.a('object');

			// Message
			expect(res.body).to.have.property('message').to.equal('Book updated!')

			// Check book properties
			expect(res.body.book).to.have.property('year').to.equal(1950)
		});
	});
	/*
	 * Test the /DELETE/:id route
	 */
	describe('/DELETE/:id book', () => {
		it('it should DELETE a book given the id', async () => {

			let book = new Book({ title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1948, pages: 778 })

			const item = await book.save(book)
			const res = await request(server).delete('/book/' + book.id)

			expect(res.statusCode).to.equal(200);
			expect(res.body).to.be.a('object');

			// Message
			expect(res.body).to.have.property('message').to.equal('Book successfully deleted!')

			// Result properties
			expect(res.body.result).to.have.property('ok').to.equal(1)
			expect(res.body.result).to.have.property('n').to.equal(1)
		});
	});
});
